#pragma once
#include "stdafx.h"
class badInput {
	std::string msg;
public:
	badInput()
		: msg("Zly input!") {}
	badInput(const std::string& err)
		: msg(err) {}
	std::string what() { return msg; }
};