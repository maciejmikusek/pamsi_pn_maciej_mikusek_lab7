#pragma once
#include "other.h"
#include <list>

class hashTable {

	friend std::ostream& operator<<(std::ostream&, const hashTable&);

private:
	std::list<int>* arrPtr;
	int size;

public:
	void test();
	hashTable();
	~hashTable() { delete[] arrPtr; }
	void pushRand();
	void push(const int&);
	void searchFor(const int&) const;
	void remove(const int&);

private: // tools here
	bool available(const int&) const; // change this for other versions ii and iii
	bool foundAtLeastOne(const int&)const; // found at least one of args in the array - true
	int hashing(const int&) const;
	
};

