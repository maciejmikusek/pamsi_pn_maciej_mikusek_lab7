#include "stdafx.h"
#include "hashTable.h"
#include "global.h"


hashTable::hashTable() { // change this for other versions
	int sizeInput;

	std::cout << std::endl << "Podaj rozmiar tablicy haszujacej (liczba pierwsza):" << std::endl;
	while (!(std::cin >> sizeInput) || (!isPrime(sizeInput))) {
		std::cin.clear();
		std::cin.ignore();
		std::cout << "Zly input! Podaj liczbe pierwsza:" << std::endl;
	}
	size = sizeInput;
	arrPtr = new std::list<int>[size];
}

bool hashTable::available(const int& index) const {
	return (arrPtr[index].empty()); // actually, in version 'i' you won't use this at all
}

void hashTable::test() {
	std::cout << "";
}

bool hashTable::foundAtLeastOne(const int& waldo)const {
	int i = hashing(waldo);
		for (std::list<int>::iterator j = arrPtr[i].begin(); j != arrPtr[i].end(); ++j) {
			if (*j == waldo) { return true; }
		}
	return false;
}

void hashTable::push(const int& input) {
	int index = hashing(input);
	if (!foundAtLeastOne(input)) {
		arrPtr[index].push_front(input);
		std::cout << "Dodano wartosc " << input << " do wiaderka w indeksie " << index << ".\n";
	}
	//else {
	//	std::cout << "Wykryto powtorzenie, co jest niedozwolone.\n";
	//}
}

int hashTable::hashing(const int& key) const {
	return key % size;
}

void hashTable::pushRand() {
	int input;
	do {
		input = rng(randomRangeBeg, randomRangeEnd);
	} while (foundAtLeastOne(input));
	push(input);
}

std::ostream& operator<<(std::ostream& os, const hashTable& obj) {
	for (unsigned int i = 0; i < obj.size; ++i) {
		os << "[" << i << "] (";
		for (std::list<int>::iterator j = obj.arrPtr[i].begin(); j != obj.arrPtr[i].end(); ++j) {
			os << " " << *j;
		}
		os << ")\n";
	}
	return os;
}

void hashTable::searchFor(const int& waldo)const {
	int foundIt = 0;
	int i = hashing(waldo);
		for (std::list<int>::iterator j = arrPtr[i].begin(); j != arrPtr[i].end(); ++j) {
			if (*j == waldo) { 
				++foundIt;
				break;
			}
		}
		//std::cout << i;			 // debug
	if (foundIt != 0) {
		std::cout << "Znaleziono wartosc " << waldo << " w wiaderku nr " << i << ".\n";
	}
	else {
		std::cout << "Nie znaleziono wartosci " << waldo << " w tablicy.\n";
	}
}

void hashTable::remove(const int& bill) {
	int foundIt = 0;
	int i = hashing(bill);
		for (std::list<int>::iterator j = arrPtr[i].begin(); j != arrPtr[i].end(); ++j) {
			if (*j == bill) {
				++foundIt;
				arrPtr[i].erase(j);
				break;
			}
		}
		//std::cout << i;			 // debug
	if (foundIt != 0) {
		std::cout << "Usunieto wartosc " << bill << " z tablicy.\n";
	}
	else {
		std::cout << "Nie znaleziono wartosci " << bill << " w tablicy.\n";
	}
}