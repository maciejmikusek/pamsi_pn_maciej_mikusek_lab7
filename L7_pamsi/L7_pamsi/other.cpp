#include "stdafx.h"
#include "other.h"

int rng(const int& beg, const int& end) {
	std::mt19937 rng;
	rng.seed(std::random_device()());
	std::uniform_int_distribution<int> dist6(beg, end);

	return dist6(rng);
}

bool isPrime(const int& number) {
	if (number < 2) return false;
	for (int i = 2; i <= sqrt(number); ++i) {
		if ((number%i) == 0) return false;
	}
	return true;
	
}