#pragma once
#include "stdafx.h"
class itsFull {
	std::string msg;
public:
	itsFull()
		: msg("It's full!!!") {}
	itsFull(const std::string& err)
		: msg(err) {}
	std::string what() { return msg; }
};

class itsEmpty {
	std::string msg;
public:
	itsEmpty()
		: msg("It's empty!!!") {}
	itsEmpty(const std::string& err)
		: msg(err) {}
	std::string what() { return msg; }
};

class badHasherInput {
	std::string msg;
public:
	badHasherInput()
		: msg("It's empty!!!") {}
	badHasherInput(const std::string& err)
		: msg(err) {}
	std::string what() { return msg; }
};