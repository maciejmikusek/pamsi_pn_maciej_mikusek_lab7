#pragma once
#include "other.h"
#include <list>

class hashTable {

	friend std::ostream& operator<<(std::ostream&, const hashTable&);

private:
	int* arrPtr;
	int size;
	int takenSize;
	int doubleHasher;

public:
	void test();
	hashTable();
	hashTable(const int&);
	~hashTable() { delete[] arrPtr; }
	void pushRand();
	void push(const int&) throw (itsFull);
	void searchFor(const int&) const;
	void remove(const int&) throw (itsEmpty);

private: // tools here
	bool available(const int&) const; // change this for other versions ii and iii
	bool foundAtLeastOne(const int&)const; // found at least one of args in the array - true
	int hashing(const int&) const;
	int hashing2(const int&) const;
	bool isFull() const;
	bool isEmpty() const;
	void setDoubleHasher(); // first prime number that is smaller than size

};