#include "stdafx.h"
#include "hashTable.h"
#include "global.h"


hashTable::hashTable() { // change this for other versions
	int sizeInput;

	std::cout << std::endl << "Podaj rozmiar tablicy haszujacej (liczba pierwsza, wieksza od 2):" << std::endl;
	while (!(std::cin >> sizeInput) || (!isPrime(sizeInput))||(sizeInput == 2)) {
		std::cin.clear();
		std::cin.ignore();
		std::cout << "Zly input! Podaj liczbe pierwsza, wieksza od 2:" << std::endl;
	}
	size = sizeInput;
	setDoubleHasher();
	//debug:
	std::cout << "doubleHasher == " << doubleHasher << "\n";
	takenSize = 0;
	arrPtr = new int[size];
	for (unsigned int i = 0; i < size; ++i) {
		arrPtr[i] = -1;
	}
}

hashTable::hashTable(const int& secondHasherInput) { // change this for other versions

		if (!isPrime(secondHasherInput)) {
			throw badHasherInput();
		}
		int sizeInput;

		std::cout << std::endl << "Podaj rozmiar tablicy haszujacej (liczba pierwsza, wieksza od 2):" << std::endl;
		while (!(std::cin >> sizeInput) || (!isPrime(sizeInput)) || (sizeInput == 2) || (!(secondHasherInput<sizeInput))) {
			std::cin.clear();
			std::cin.ignore();
			if (!(secondHasherInput < sizeInput)) {
				std::cout << "Zly input! Rozmiar musi byc wiekszy od " << secondHasherInput << "!\n";
			}
			else {
				std::cout << "Zly input! Podaj liczbe pierwsza, wieksza od 2:" << std::endl;
			}
		}
		size = sizeInput;
		doubleHasher = secondHasherInput;
		//debug:
		std::cout << "doubleHasher == " << doubleHasher << "\n";
		takenSize = 0;
		arrPtr = new int[size];
		for (unsigned int i = 0; i < size; ++i) {
			arrPtr[i] = -1;
		}
}

bool hashTable::available(const int& index) const {
	return (arrPtr[index] == -1);
}

void hashTable::test() {
	std::cout << "";
}

bool hashTable::foundAtLeastOne(const int& waldo)const {
	int index;
	int helper = 0;
	int h1 = hashing(waldo);
	int h2 = hashing2(waldo);
	for (unsigned int j = 0; j < size; ++j) {

		index = (h1 + j * h2) % size;

		if (arrPtr[index] == waldo) { return true; }
		if (helper == size) break;
		++helper;
	}
	return false;
}

void hashTable::push(const int& input) throw (itsFull) {
	try
	{
		int ind;
		if (isFull()) throw itsFull();
		int konflikt;
		int h1 = hashing(input);
		int h2 = hashing2(input);
		if (!foundAtLeastOne(input) && input!=0 && input!=-1) {
			for (unsigned int j = 0; j < size; ++j) {

				ind = (h1 + j * h2) % size;

				if (available(ind)) {
					arrPtr[ind] = input;
					konflikt = j;
					break;
				}
			}

			
			takenSize++;
			if (konflikt > 0)
			std::cout << "\nWystapil konflikt, j ma wartosc " << konflikt << ".\n";
			std::cout << "Dodano wartosc " << input << " do pola nr " << ind << ".\n";
		}
		//else {
		//	std::cout << "Wykryto powtorzenie, co jest niedozwolone.\n";
		//}
	}
	catch (itsFull& obj)
	{
		std::cout << obj.what() << std::endl;
	}
}

int hashTable::hashing(const int& key) const {
	return key % size;
}

int hashTable::hashing2(const int& key) const {
	return doubleHasher - (key % doubleHasher);
}


void hashTable::pushRand() {
	int input;
	do {
		input = rng(randomRangeBeg, randomRangeEnd);
	} while (foundAtLeastOne(input));
	push(input);
}

std::ostream& operator<<(std::ostream& os, const hashTable& obj) {
	for (unsigned int i = 0; i < obj.size; ++i) {
		os << "[" << i << "] ";
		//os << "[ ";
		if (!obj.available(i)) {
			os << obj.arrPtr[i];
		}
		//os << " ]";
		os << "\n";
	}
	return os;
}

void hashTable::searchFor(const int& waldo)const {
	int helper = 0;
	int foundIt = 0;
	int ind;
	int i = hashing(waldo);
	int h2 = hashing2(waldo);
	for (unsigned int j = 0; j < size; ++j) {

		ind = (i + j*h2) % size;

		if (arrPtr[ind] == waldo) {
			++foundIt;
			break;
		}
		if (helper == size) break;
		helper++;
	}
	//std::cout << i;			 // debug
	if (foundIt != 0) {
		std::cout << "Znaleziono wartosc " << waldo << " w polu nr " << ind << ".\n";
	}
	else {
		std::cout << "Nie znaleziono wartosci " << waldo << " w tablicy.\n";
	}
}

void hashTable::remove(const int& bill) throw (itsEmpty) {
	try
	{
		if (isEmpty()) throw itsEmpty();
		int helper = 0;
		int foundIt = 0;
		int ind;
		int h1 = hashing(bill);
		int h2 = hashing2(bill);
		for (unsigned int j = 0; j < size; ++j) {

			ind = (h1 + j*h2) % size;

			if (arrPtr[ind] == bill) {
				++foundIt;
				arrPtr[ind] = -1;
				break;
			}
			if (helper == size) break;
			++helper;
		}
		//std::cout << i;			 // debug
		if (foundIt != 0) {
			std::cout << "Usunieto wartosc " << bill << " z tablicy.\n";
			takenSize--;
		}
		else {
			std::cout << "Nie znaleziono wartosci " << bill << " w tablicy.\n";
		}
	}
	catch (itsEmpty& obj)
	{
		std::cout << obj.what() << std::endl;
	}
}

bool hashTable::isFull() const {
	return takenSize == size;
}

bool hashTable::isEmpty() const {
	return takenSize == 0;
}

void hashTable::setDoubleHasher() {
	doubleHasher = size - 1;
	while (!isPrime(doubleHasher)) {
		--doubleHasher;
	}
}