#include "stdafx.h"
#include "hashTable.h"
#include "global.h"


hashTable::hashTable() { // change this for other versions
	int sizeInput;

	std::cout << std::endl << "Podaj rozmiar tablicy haszujacej (liczba pierwsza):" << std::endl;
	while (!(std::cin >> sizeInput) || (!isPrime(sizeInput))) {
		std::cin.clear();
		std::cin.ignore();
		std::cout << "Zly input! Podaj liczbe pierwsza:" << std::endl;
	}
	size = sizeInput;
	takenSize = 0;
	arrPtr = new int[size];
	for (unsigned int i = 0; i < size; ++i) {
		arrPtr[i] = -1;
	}
}

bool hashTable::available(const int& index) const {
	return (arrPtr[index] == -1);
}

void hashTable::test() {
	std::cout << "";
}

bool hashTable::foundAtLeastOne(const int& waldo)const {
	int helper = 0;
	int i = hashing(waldo);
	for (unsigned int j = i; 1; j = (j+1)%size) {
		if (arrPtr[j] == waldo) { return true; }
		if (helper == size) break;
		++helper;
	}
	return false;
}

void hashTable::push(const int& input) throw (itsFull) {
	try
	{
		if (isFull()) throw itsFull();
		int index = hashing(input);
		if (!foundAtLeastOne(input) && input!=-1) {
			for (unsigned int j = index; 1; j = (j + 1) % size) {
				if (available(j)) {
					arrPtr[j] = input;
					index = j;
					break;
				}
			}


			takenSize++;
			std::cout << "Dodano wartosc " << input << " do pola nr " << index << ".\n";
		}
		//else {
		//	std::cout << "Wykryto powtorzenie, co jest niedozwolone.\n";
		//}
	}
	catch (itsFull& obj)
	{
		std::cout << obj.what() << std::endl;
	}
}

int hashTable::hashing(const int& key) const {
	return key % size;
}

void hashTable::pushRand() {
	int input;
	do {
		input = rng(randomRangeBeg, randomRangeEnd);
	} while (foundAtLeastOne(input));
	push(input);
}

std::ostream& operator<<(std::ostream& os, const hashTable& obj) {
	for (unsigned int i = 0; i < obj.size; ++i) {
		os << "[" << i << "] ";
		if (obj.available(i) ){
			os << "[]";
		}
		else {
			os << obj.arrPtr[i];
		}
		os << "\n";
	}
	return os;
}

void hashTable::searchFor(const int& waldo)const {
	int helper = 0;
	int foundIt = 0;
	int i = hashing(waldo);
	for (unsigned int j = i; 1; j = (j + 1) % size) {
		if (arrPtr[j] == waldo) {
			++foundIt;
			i = j;
			break;
		}
		if (helper == size) break;
		helper++;
	}
	//std::cout << i;			 // debug
	if (foundIt != 0) {
		std::cout << "Znaleziono wartosc " << waldo << " w polu nr " << i << ".\n";
	}
	else {
		std::cout << "Nie znaleziono wartosci " << waldo << " w tablicy.\n";
	}
}

void hashTable::remove(const int& bill) throw (itsEmpty) {
	try
	{
		if (isEmpty()) throw itsEmpty();
		int helper = 0;
		int foundIt = 0;
		int i = hashing(bill);
		for (unsigned int j = i; 1; j = (j + 1) % size) {
			if (arrPtr[j] == bill) {
				++foundIt;
				arrPtr[j] = -1;
				break;
			}
			if (helper == size) break;
			++helper;
		}
		//std::cout << i;			 // debug
		if (foundIt != 0) {
			std::cout << "Usunieto wartosc " << bill << " z tablicy.\n";
			takenSize--;
		}
		else {
			std::cout << "Nie znaleziono wartosci " << bill << " w tablicy.\n";
		}
	}
	catch (itsEmpty& obj)
	{
		std::cout << obj.what() << std::endl;
	}
}

bool hashTable::isFull() const {
	return takenSize == size;
}

bool hashTable::isEmpty() const {
	return takenSize == 0;
}