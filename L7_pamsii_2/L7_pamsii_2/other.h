#pragma once
#include "stdafx.h"
#include <random>
#include "exceptions.h"

int rng(const int&, const int&);
bool isPrime(const int&);